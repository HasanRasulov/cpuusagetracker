#ifndef __ANALYZER__H__
#define __ANALYZER__H__

#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "logger.h"
#include "queue.h"

typedef struct
{

   uint64_t user, nice, system, idle, iowait, irq, softirq, steal, guest;

} cpu_stat_t;

extern SyncQueue *reader_queue;

SyncQueue *analyzer_queue;

static uint8_t calculate_usage_in_percentage(const cpu_stat_t *, const cpu_stat_t *);

void *analyze_cpu_stat(void *);

#endif
