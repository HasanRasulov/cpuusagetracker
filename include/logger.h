#ifndef __LOGGER__H__
#define __LOGGER__H__

#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "queue.h"

#define LOG_FILENAME "CPUUsageTracker.log"

typedef enum
{
      INFO,
      WARNING,
      ERROR
} LogLevel;

SyncQueue *logger_queue;

void write_log(LogLevel, const char *, const char *);
void *save_log(void *);

#endif
