#ifndef __PRINTER__H__
#define __PRINTER__H__

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include "logger.h"

extern SyncQueue *analyzer_queue;

void *print_cpu_stat(void *);

#endif