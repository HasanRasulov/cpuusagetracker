#ifndef __QUEUE__H__
#define __QUEUE__H__

#include <pthread.h>
#include <stdlib.h>
#include <stdbool.h>

struct Node
{

  void *data;
  struct Node *next;
};

typedef struct Node Node;

typedef struct
{

  Node *head;
  pthread_mutex_t mutex;
  pthread_cond_t cond;

} SyncQueue;

SyncQueue *init_queue();
void add(SyncQueue *, void *);
void *pop(SyncQueue *);
void destroy(SyncQueue *);
#endif
