#ifndef __READER__H__
#define __READER__H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "queue.h"
#include "logger.h"

#define LINE_LEN 1024

static const char *stat_path = "/proc/stat";

SyncQueue *reader_queue;

void *read_cpu_stat(void *);

#endif
