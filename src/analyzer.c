#include "analyzer.h"

uint8_t calculate_usage_in_percentage(const cpu_stat_t *prev_stat, const cpu_stat_t *cur_stat)
{

    uint64_t prev_idle = prev_stat->idle + prev_stat->iowait;
    uint64_t cur_idle = cur_stat->idle + cur_stat->iowait;

    uint64_t prev_total = prev_idle + prev_stat->user + prev_stat->nice + prev_stat->system + prev_stat->irq + prev_stat->softirq + prev_stat->steal;
    uint64_t cur_total = cur_idle + cur_stat->user + cur_stat->nice + cur_stat->system + cur_stat->irq + cur_stat->softirq + cur_stat->steal;

    uint64_t actual_total = cur_total - prev_total;
    uint64_t actual_idle = cur_idle - prev_idle;

    return (uint8_t)((actual_total - actual_idle) * 100 / actual_total);
}

void *analyze_cpu_stat(void *arg)
{

    long number_of_processors = sysconf(_SC_NPROCESSORS_ONLN);

    char *cpu_stat = NULL;
    analyzer_queue = init_queue();

    cpu_stat_t *prev_stat = (cpu_stat_t *)malloc(sizeof(cpu_stat_t) * number_of_processors);
    long i;

    for (i = 0; i < number_of_processors; i++)
    {

        cpu_stat = (char *)pop(reader_queue);
        write_log(INFO, cpu_stat, __FILE__);

        sscanf(cpu_stat, "%*s %lu %lu %lu %lu %lu %lu %lu %lu", &prev_stat[i].user, &prev_stat[i].nice, &prev_stat[i].system, &prev_stat[i].idle, &prev_stat[i].iowait, &prev_stat[i].irq, &prev_stat[i].softirq, &prev_stat[i].steal);

        free(cpu_stat);
    }

    cpu_stat_t *cur_stat = (cpu_stat_t *)malloc(sizeof(cpu_stat_t) * number_of_processors);

    while (1)
    {

        uint8_t *processor_usage = (uint8_t *)malloc(sizeof(uint8_t) * number_of_processors);

        for (i = 0; i < number_of_processors; i++)
        {
            cpu_stat = (char *)pop(reader_queue);
            write_log(INFO, cpu_stat, __FILE__);

            sscanf(cpu_stat, "%*s %lu %lu %lu %lu %lu %lu %lu %lu", &cur_stat[i].user, &cur_stat[i].nice, &cur_stat[i].system, &cur_stat[i].idle, &cur_stat[i].iowait, &cur_stat[i].irq, &cur_stat[i].softirq, &cur_stat[i].steal);

            processor_usage[i] = calculate_usage_in_percentage(&prev_stat[i], &cur_stat[i]);

            add(analyzer_queue, processor_usage);
            free(cpu_stat);
        }

        memcpy(prev_stat, cur_stat, sizeof(cpu_stat_t));
        sleep(1);
    }

    free(cur_stat);
    free(prev_stat);
    destroy(reader_queue);
    return NULL;
}
