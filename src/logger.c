#include "logger.h"

void *save_log(void *args)
{

   logger_queue = init_queue();
   FILE *log_file = fopen(LOG_FILENAME, "w");

   if (!log_file)
   {
      puts("Log file could not be opened");
      exit(1);
   }

   while (1)
   {
      char *line = (char *)pop(logger_queue);

      if (fputs(line, log_file) == EOF)
         puts("ERROR has occured while saving log!!!");
      free(line);
   }

   fclose(log_file);

   destroy(logger_queue);
   return NULL;
}

void write_log(LogLevel level, const char *message, const char *filename)
{

   char *log = (char *)malloc(strlen(message) + strlen(filename) + 20);

   if (level == INFO)
   {
      strcat(log, "INFO: ");
   }
   else if (level == WARNING)
   {
      strcat(log, "WARNING: ");
   }
   else
   {
      strcat(log, "ERROR: ");
   }

   strcat(log, filename);
   strcat(log, ": ");

   strcat(log, message);

   add(logger_queue, log);
}
