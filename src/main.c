#include "analyzer.h"
#include "reader.h"
#include "printer.h"

#include <signal.h>
#include "logger.h"

pthread_t logger_thread, reader_thread, analyzer_thread, printer_thread;

void sigterm_handler(int signum)
{
   puts("Process has received SIGTERM!!!");
   pthread_cancel(logger_thread);
   pthread_cancel(reader_thread);
   pthread_cancel(analyzer_thread);
   pthread_cancel(printer_thread);
}

int main()
{

   struct sigaction action;
   memset(&action, 0, sizeof(struct sigaction));
   action.sa_handler = sigterm_handler;
   sigaction(SIGTERM, &action, NULL);
   sigaction(SIGINT, &action, NULL);

   pthread_create(&logger_thread, NULL, &save_log, NULL);
   usleep(1);
   pthread_create(&reader_thread, NULL, &read_cpu_stat, NULL);
   usleep(1);
   pthread_create(&analyzer_thread, NULL, &analyze_cpu_stat, NULL);
   usleep(1);
   pthread_create(&printer_thread, NULL, &print_cpu_stat, NULL);

   pthread_join(logger_thread, NULL);
   pthread_join(reader_thread, NULL);
   pthread_join(analyzer_thread, NULL);
   pthread_join(printer_thread, NULL);

   return EXIT_SUCCESS;
}
