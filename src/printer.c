#include "printer.h"

void *print_cpu_stat(void *arg)
{

    long number_of_processors = sysconf(_SC_NPROCESSORS_ONLN);

    uint8_t *processor_usage;
    size_t i;

    while (1)
    {

        uint16_t total_usage = 0;

        processor_usage = (uint8_t *)pop(analyzer_queue);

        for (i = 0; i < number_of_processors; i++)
        {
            total_usage += processor_usage[i];
        }

        free(processor_usage);
        total_usage /= number_of_processors;

        printf("Total usage:%u\n", total_usage);
    }

    destroy(analyzer_queue);
    return NULL;
}