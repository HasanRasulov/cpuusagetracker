#include "queue.h"

SyncQueue *init_queue()
{

  SyncQueue *queue = (SyncQueue *)malloc(sizeof(SyncQueue));

  queue->head = NULL;
  pthread_mutex_init(&queue->mutex, NULL);
  pthread_cond_init(&queue->cond, NULL);

  return queue;
}

void add(SyncQueue *queue, void *data)
{

  pthread_mutex_lock(&queue->mutex);

  if (queue->head == NULL)
  {
    queue->head = (Node *)malloc(sizeof(Node));
    queue->head->data = data;
    queue->head->next = NULL;
  }
  else
  {

    Node *head = queue->head;
    while (head->next != NULL)
    {
      head = head->next;
    }
    head->next = (Node *)malloc(sizeof(Node));
    head->next->data = data;
    head->next->next = NULL;
  }

  pthread_cond_signal(&queue->cond);

  pthread_mutex_unlock(&queue->mutex);
}

void *pop(SyncQueue *queue)
{

  void *data = NULL;

  pthread_mutex_lock(&queue->mutex);

  while (queue->head == NULL)
  {
    pthread_cond_wait(&queue->cond, &queue->mutex);
  }

  Node *old_head = queue->head;
  data = queue->head->data;
  queue->head = queue->head->next;
  free(old_head);

  pthread_mutex_unlock(&queue->mutex);

  return data;
}

void destroy(SyncQueue *queue)
{

  pthread_mutex_destroy(&queue->mutex);
  pthread_cond_destroy(&queue->cond);
  free(queue);
}
