#include "reader.h"

void *read_cpu_stat(void *arg)
{

  long number_of_processors = sysconf(_SC_NPROCESSORS_ONLN);

  const char *keyword = "cpu";
  reader_queue = init_queue();
  char buffer[LINE_LEN];

  while (1)
  {

    FILE *stat_fp = fopen(stat_path, "r");

    if (!stat_fp)
    {
      write_log(ERROR, "Stat file could not be opened", __FILE__);
      exit(1);
    }

    {
      /* ignore first line */
      fgets(buffer, LINE_LEN, stat_fp);
      write_log(INFO, buffer, __FILE__);
    }

    for (size_t i = 0; i < number_of_processors; i++)
    {

      char *line = (char *)malloc(sizeof(char) * LINE_LEN);

      if (fgets(line, LINE_LEN - 1, stat_fp) != NULL && !strncmp(line, keyword, strlen(keyword)))
      {

        add(reader_queue, line);
        write_log(INFO, line, __FILE__);
      }
    }

    fclose(stat_fp);
    sleep(1);
  }
  return NULL;
}
